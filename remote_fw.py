# used to upload a binary to a RPi and upload it ath the same time
import json
from pathlib import Path
from upload_file import upload
from run_remote_py import run_remote
import paramiko


def sftp_path_exists(sftp, path):
    try:
        sftp.stat(path)
        return True
    # doing (Exception, IOError, FileNotFoundError) doesnt work seems sth is borked in the package
    except IOError as e:
        return False


# recursive mkdir
def mkdir_p(sftp, remote_directory):
    for parent in reversed(remote_directory.parents):
        if not sftp_path_exists(sftp, parent.as_posix()):
            sftp.mkdir(parent.as_posix())


def upload(ssh, upload_path, upload_target, executeable, ignore_hidden_files, ignore_folders, ignore_filetypes):

    if not (upload_target.is_dir() or upload_target.is_file()):
        print("invalid upload target passed {}".format(upload_target))
        return False

    if executeable and not executeable.is_file():
        print("invalid executeable passed")
        return False

    try:
        sftp = ssh.open_sftp()
    except OSError as OSE:
        print("failed to get SFTP")
        print(OSE)
        return False

    mkdir_p(sftp, upload_path)
    files_to_upload = upload_target
    mkdir_p(sftp, upload_path)

    print("\n--- uploading ... --- \n")

    sftp.put(local_file.as_posix(), remote_file.as_posix())

    print("upload of folder {} complete".format(upload_target))
    return True


def main():
    # comments on what each parameter is for is in the json
    try:
        with open('fw_config.json') as json_file:
            cfg = json.loads(json_file.read())
            pi_addr = cfg["pi_addr"]
            username = cfg["username"]
            password = cfg["password"]
            upload_path = Path(cfg["upload_path"])
            upload_target = Path(cfg["upload_target"])
            executeable = Path(cfg["executeable"])
    except ValueError as e:
        print('failure to read config.json')
        print(e)
        input("Press any key to exit")
        exit()

    if upload_path == Path(""):
        print("missing upload path specified")
        exit()
    if upload_target == Path(""):
        print("missing upload target")
        exit()

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(pi_addr, username=username, password=password)
    except:
        print("Failed to connect, terminating")
        return False

    if not upload(ssh, upload_path, upload_target, executeable):
        print("Upload failed")
        ssh.close()
        return False

