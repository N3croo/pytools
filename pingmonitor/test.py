from pythonping import ping
from time import sleep
from datetime import datetime as dt
import json
import matplotlib.pyplot as plt
from time import sleep


pings = []
times = []

def main():
    try:
        with open('config.json') as json_file:
            cfg = json.loads(json_file.read())
            # times should be given in seconds, will be converted to ms to work with dt
            intervals = cfg.get("interval", 15)
            history_length = cfg.get("history_length", 300) * 1000
            ping_targets = cfg.get("ping_targets", ["192.168.178.1"])
    except FileNotFoundError as e:
        print('failure to read config.json')
        print(e)
        input("Press any key to exit")
        exit()

    fig, ax = plt.subplots()
    i = 0
    while i < 5:
        print("mainloop")
        res = ping('192.168.178.1', verbose=True)
        times.append(dt.now())
        pings.append(res.rtt_max_ms)
        plt.plot(times, pings)
        print("plotting")
        sleep(5)
        print("mainloop end")
        i += 1
    plt.show(block=True)


    '''
    https://matplotlib.org/2.0.2/examples/api/date_demo.html
    import matplotlib.dates as mdates
    myFmt = mdates.DateFormatter('%d')
    ax.xaxis.set_major_formatter(myFmt)
    '''


main()

'''
res = ping('192.168.178.1', verbose=True)
print(res.rtt_max_ms)
print(res.packet_loss)
print(res.rtt_avg_ms)
print(len(res))
print(res.__iter__())

'''

