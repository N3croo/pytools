import paramiko
import json
from pathlib import Path
from upload_file import upload
from run_remote_py import run_remote
from qry_ip import qry_ip

# todo:
# - add parameter to have this script called from console parameter?
# check pathlib implementation


def main():
    # comments on what each parameter is for is in the json
    try:
        with open('config.json') as json_file:
            cfg = json.loads(json_file.read())
            pi_addr = cfg["pi_addr"]
            username = cfg["username"]
            password = cfg["password"]
            upload_path = Path(cfg["upload_path"])
            upload_target = Path(cfg["upload_target"])
            executeable = Path(cfg["executeable"])
            timeout = cfg["timeout"]
            ignore_hidden_files = cfg["ignore_hidden_files"]
            ignore_folders = cfg["ignore_folders"]
            ignore_filetypes = cfg["ignore_filetypes"]
    except ValueError as e:
        print('failure to read config.json')
        print(e)
        input("Press any key to exit")
        exit()

    if upload_path == Path(""):
        print("missing upload path specified")
        exit()
    if upload_target == Path(""):
        print("missing upload target")
        exit()

    if upload_target not in executeable.parents:
        print("executeable not inside uploadtarget")
        # exit()

    if not upload_target.is_file():
        upload_path = upload_path.joinpath(upload_target.name)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(pi_addr, username=username, password=password)
    except:
        print("Failed to connect, terminating")
        return False

    if not upload(ssh, upload_path, upload_target, executeable, ignore_hidden_files, ignore_folders, ignore_filetypes):
        print("Upload failed")
        ssh.close()
        return False

    if not run_remote(ssh, executeable, upload_path, upload_target, timeout):
        print("execution failed")
        ssh.close()
        return False

    ssh.close()


main()
