import subprocess
from platform import system


def qry_ip(ip, no_retries=4):
    sys = system()
    if sys == "Windows":
        qty_param = "-n"
    elif sys == "Linux":
        qty_param = "-c"
    else:
        print("failed to get OS, exiting")
        return False
    no_retries = str(int(no_retries))
    res = subprocess.call(['ping', qty_param, no_retries, ip])
    if res == 0:
        print("ping to {} OK".format(ip))
        return True
    elif res == 2:
        print("no response from " + ip)
    else:
        print("ping to {} failed!".format(ip))
    return False


if __name__ == "__main__":
    print(qry_ip("192.168.178.49", 10))
