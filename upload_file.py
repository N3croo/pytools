import paramiko
from re import sub, match
from pathlib import Path
import os


# TODO: check if passing a file as uploadtarget works

def get_dirs_content(path, ignore_hidden=False, ignore_folders=[], ignore_filetypes=[]):
    dirs = []
    files = []
    for content in path.iterdir():

        if content.is_dir():
            if ignore_hidden and match("\.", content.name) is not None:
                print(content)
                print("HIDDEN")
                continue
            for ignored in ignore_folders:
                if match(content.name, ignored):
                    print(content)
                    print("IGRNORED")
                    continue
            dirs.append(content)
            cdirs, cfiles = get_dirs_content(content)
            dirs.extend(cdirs)
            files.extend(cfiles)
        else:
            for file in files:
                if file.suffix in ignore_filetypes:
                    files.remove(file)
            files.append(content)
    return dirs, files


def sftp_path_exists(sftp, path):
    try:
        sftp.stat(path)
        return True
    # doing (Exception, IOError, FileNotFoundError) doesnt work seems sth is borked in the package
    except IOError as e:
        return False


# recursive mkdir
def mkdir_p(sftp, remote_directory):
    for parent in reversed(remote_directory.parents):
        if not sftp_path_exists(sftp, parent.as_posix()):
            sftp.mkdir(parent.as_posix())
            sftp.chmod(parent.as_posix, 777)


# accept a prexisting ssh on pi_data is also possible, we dont need to connect and disconnect multiple times
def upload(pi_data, upload_path, upload_target, executeable, ignore_hidden_files, ignore_folders, ignore_filetypes):

    if not (upload_target.is_dir() or upload_target.is_file()):
        print("invalid upload target passed {}".format(upload_target))
        return False

    if executeable and not executeable.is_file():
        print("invalid executeable passed")
        return False

    close_ssh = False

    if type(pi_data) is paramiko.SSHClient:
        ssh = pi_data
    else:
        close_ssh = True
        ssh = paramiko.SSHClient()
        pi_addr, username, password = pi_data
        # you may need to uncomment this line the first time
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(pi_addr, username=username, password=password)
        except paramiko.ssh_exception as SSH_exp:
            print(SSH_exp)
            return False

    try:
        sftp = ssh.open_sftp()
    except OSError as OSE:
        print("failed to get SFTP")
        print(OSE)
        return False

    mkdir_p(sftp, upload_path)
    # https://stackoverflow.com/questions/14819681/upload-files-using-sftp-in-python-but-create-directories-if-path-doesnt-exist

    print("\n--- checking and creating folders ---\n")
    folders_to_create = []
    if not upload_target.is_file():
        folders_to_create, files_to_upload = get_dirs_content(upload_target, ignore_hidden_files, ignore_folders, ignore_filetypes)
    else:
        files_to_upload = [upload_target]

    for folder in folders_to_create:
        folder = sub(upload_target.as_posix(), upload_path.as_posix(), folder.as_posix())
        print(folder)
        try:
            exists = sftp.stat(folder)
            print("folder {} exists already".format(folder))
        # doing (Exception, IOError, FileNotFoundError) doesnt work seems sth is borked in the package
        except IOError as e:
            exists = False
            print("folder does not exists")
        if not exists:
            ret = sftp.mkdir(folder)
            print(ret)

    print("\n--- uploading ... --- \n")

    for local_file in files_to_upload:
        local_mtime = os.lstat(local_file).st_mtime
        if upload_target.is_file():
            remote_file = upload_path.joinpath(upload_target.name)
        else:
            remote_file = local_file.relative_to(upload_target)
            remote_file = upload_path.joinpath(remote_file)
        try:
            print(remote_file.as_posix())
            # https://stackoverflow.com/questions/15481934/ioerror-errno-2-no-such-file-paramiko-put
            # paramkio exception handling leaves things to be desired,
            # other issues are reported as a FNF Error or expections occur during expections...
            # either TODO: a missing parent folder
            # permission problem here but it should be done unless you had somebody create folder
            # without perms if so thats hopefully with reason

            file_stat = sftp.lstat(remote_file.as_posix())

            # TODO: check whats up with the files not updating
            if file_stat.st_mtime < local_mtime:
                print("{} has been changed, updating".format(remote_file.as_posix()))

                sftp.put(local_file.as_posix(), remote_file.as_posix())
                # print("Upload of {} complete".format(tgt))
            else:
                print("not uploading {}, no changes have been made".format(remote_file))

        # in case you get a magical filenotfounderror that breaks the excpection you may miss files folder
        except (OSError, FileNotFoundError) as e:
            print(e)
            sftp.put(local_file.as_posix(), remote_file.as_posix())
            print("Upload of {} complete".format(remote_file))
        except:
            print("fucking exceptions")

    if close_ssh:
        print("closing")
        ssh.close()

    print("upload of folder {} complete".format(upload_target))
    return True

