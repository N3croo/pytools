from scapy.all import IP, ICMP, sr1


def ping(ip_addr, timeout=2):
    # ip = sys.argv[1]    # command line argument
    # IP defines the protocol for IP addresses
    # dst is the destination IP address
    # TCP defines the protocol for the ports
    icmp = IP(dst=ip_addr) / ICMP()
    resp = sr1(icmp, timeout=timeout)
    print("pinging: {}".format(ip_addr))
    if resp is None:
        print("This host is down")
        return False
    else:
        print("response from: {}".format(resp.src))
        print("This host is up")
        return True


if __name__ == "__main__":
    # cannot ping 127.0.0.1?
    ping("192.168.178.49", 2)


