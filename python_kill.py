import paramiko
import json


def main():
    try:
        with open('config.json') as json_file:
            cfg = json.loads(json_file.read())
            pi_addr = cfg["pi_addr"]
            username = cfg["username"]
            password = cfg["password"]
    except ValueError as e:
        print('failure to read config.json')
        print(e)
        input("Press any key to exit")
        exit()

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(pi_addr, username=username, password=password)
    except:
        print("Failed to connect, terminating")
        return False
    stdin, stdout, stderr = ssh.exec_command("sudo pkill python")
    stdin, stdout, stderr = ssh.exec_command("sudo pkill python3")


main()
