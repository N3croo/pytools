from pathlib import Path
from re import match

test_path = Path("H:\\teamescape\\drive\\web_stb_override")


def get_dirs_content(path, ignore_hidden=False, ignore_folders=[]):
    dirs = []
    files = []
    for content in path.iterdir():

        if content.is_dir():
            if ignore_hidden and match("\.", content.name) is not None:
                print("HIDDEN")
                continue
            for ignored in ignore_folders:
                if match(content.name, ignored):
                    print(content)
                    print("IGRNORED")
                    continue
            dirs.append(content)
            cdirs, cfiles = get_dirs_content(content)
            dirs.extend(cdirs)
            files.extend(cfiles)
        else:
            files.append(content)
    return dirs, files


dirs, files = get_dirs_content(test_path)