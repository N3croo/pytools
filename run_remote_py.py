import json
import paramiko
from pathlib import Path
from time import time


# Todo: kill process after timeout?


# pi_data also accepts ssh connections
def run_remote(pi_data, run_file, upload_path, upload_target, timeout):
    print("attempting to run {} remotely".format(run_file))
    close_ssh = False
    if type(pi_data) is paramiko.SSHClient:
        ssh = pi_data
    else:
        close_ssh = True
        ssh = paramiko.SSHClient()
        pi_addr, username, password = pi_data
        # you may need to uncomment this line the first time
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(pi_addr, username=username, password=password)
        except paramiko.ssh_exception as SSH_exp:
            print(SSH_exp)
            return False

    ssh.invoke_shell()
    remote_file = Path(run_file).relative_to(upload_target)
    upload_path = Path(upload_path)
    remote_file = upload_path.joinpath(remote_file)
    print(remote_file)
    stdin, stdout, stderr = ssh.exec_command("cd {}; sudo python3 {}".format(remote_file.parents[0].as_posix(),
                                                                             remote_file.as_posix()))
    # The output only comes out only once the script terminated
    # stderr.channel.exit_status_ready():
    start_time = time()
    while start_time + timeout > time():
        err = stderr.read().decode("utf-8")
        if err:
            print(err)
        ret = stdout.read().decode("utf-8")
        if ret:
            print(ret)

    if stdout.channel.exit_status_ready():
        print("exited due to timeout")

    if close_ssh:
        ssh.close()

    return True

